import request from "@/utils/request";
import { getToken, getRefreshToken } from '@/utils/auth'

// 登录并获得token
export function login(data) {
    return request.post("/users/token", data);
}

// 不登录的情况下刷新token
export function refreshToken() {
    let accessToken = getToken();
    let refreshToken = getRefreshToken();
    let data = {
        accessToken,
        refreshToken
    }
    if (accessToken && refreshToken) {
        return request.post("/users/refreshtoken", data);
    } else {
        return false;
    }
}

// 获取用户列表
export function getUsers(params) {
    return request.get('/users', { params });
}

// 新增用户
export function addUser(data) {
    return request.post('/users', data);
}

// 修改用户
export function modUser(id, data) {
    return request.put(`/users/${id}`, data);
}

// 删除用户
export function delUser(id) {
    return request.delete(`/users/${id}`);
} 