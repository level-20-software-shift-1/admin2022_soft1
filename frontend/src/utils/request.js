import axios from 'axios'
import { getToken, getRefreshToken,setToken,clearToken } from '@/utils/auth'
import appConfig from '@/config/appConfig'
import {refreshToken} from '@/api/userApi'
import { Message } from 'element-ui'

const instance = axios.create({
    baseURL: appConfig.baseUrl,
    timeout: 5000
});


// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    let token = getToken();
    if (token) {
        config.headers['Authorization'] = 'Bearer ' + token
    }

    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    console.log(response);

    // 对响应数据做点什么
    return response.data;
}, function (error) {
    // 对响应错误做点什么
    // 后端原来返回的是一个401网络错误，无法捕获401错误，对后端token未通过验证进行自定义返回信息就可以捕获401错误
    if (error.response.status === 401) {
        Message.info('当前登录已经失效，正在为你重新登录，请稍候。。。')
        let param = {
            token: getToken(),
            refreshToken: getRefreshToken()
        }

        // 如果可以重新刷新获得token，则继续返回原来要进行的请求，否则清空token和refreshToken并返回错误
        refreshToken(param).then(res => {
            console.log(res);
            setToken(res.data.accessToken, res.data.refreshToken)
            Message({
                type: 'success',
                message: res.msg + ' 请刷新页面或者点击页面中的刷新按钮'
            })
            // 刷新token后，继续执行原来（想要执行的）请求
            return instance.request(error.config);
        }).catch(err => {
            Message({type:'error',message:err.Message})
            // console.log(err);
            clearToken()
        })

    }
    return Promise.reject(error);
});

export default instance