import appConfig from '@/config/appConfig'

export function showPicture(fileId) {
    let showUrl = `/files/${fileId}`
    return appConfig.baseUrl + showUrl
}