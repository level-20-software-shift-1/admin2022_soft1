import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import { isLogin } from '@/utils/auth'

Vue.use(VueRouter);


// 3. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const router = new VueRouter({
    mode: 'history',
    routes // (缩写) 相当于 routes: routes
})

router.beforeEach((to, from, next) => {
    let isAuth = isLogin();

    // 当前为登录状态（有token，需要注意的是有token不一定有效）
    if (isAuth) {
        if (to.path === '/login') {
            next('/')
        } else {
            next();
        }
    } else {
        if (to.path === '/login') {
            next();
        } else {
            next('/login');
        }
    }
})

export default router