import LayoutBase from '../components/LayoutBase'
import NotFound from '../components/NotFound'
// import UserPosts from '../components/UserPosts'


const routes = [
  {
    path: "/",
    meta: {
      title: '首页',
      icon: '',
      hidden:true
    },
    redirect:'/dashboard',
    component: LayoutBase,
    children: [
      {
        path: '/dashboard',
        meta: {
          title: '仪表盘'
        },
        component: () => import('../components/UserPosts')
      }
    ]
  },
  {
    path: "/article",
    meta: {
      title: "文章管理",
    },
    component: LayoutBase,
    children: [
      {

        path: "/article/category",
        meta: {
          title: "类别管理",
        },
        component: () => import('../views/ArticleCategory')
      },
      {
        path: "/article/author",
        meta: {
          title: "作者管理",
        },
        component: () => import('../components/UserPosts')
      },
      {
        path: "/article/article",
        meta: {
          title: "文章管理",
        },
        component: () => import('../components/UserPosts')
      },
    ],
  },
  {

    path: "/system",
    meta: {
      title: "系统设置",
    },
    component:LayoutBase,
    children: [
      {

        path: "/system/user",
        meta: {
          title: "用户管理",
        },
        component:()=>import('@/views/UserCenter')
        
      },
      {

        path: "/system/avatar",
        meta: {
          title: "头像管理",
        },
        component:()=>import('@/views/AvatarView')

      },
      {

        path: "/system/role",
        meta: {
          title: "角色管理",
        }
      },
      {

        path: "/system/permission",
        meta: {
          title: "权限管理",
        },
        component: () => import('../components/UserPosts')
      },
    ],
  },
  {
    path:'/usercenter',
    meta:{      
      hidden:true
    },
    component:LayoutBase,
    children:[
      {
        path:'notice',
        meta:{
          title:'通知信息',
          hidden:true
        },
        component:()=>import('../views/UserNotice')
      },
      {
        path:'center',
        meta:{
          title:'个人中心',
          hidden:true
        },
        component:()=>import('../views/UserCenter')
      },
    ]
  },
  {
    path: '/login',
    meta: {
      title: '登录',
      hidden: true
    },
    component: () => import('../views/LoginView')
  },
  {
    path: '*',
    meta: {
      title: '你看不到我',
      hidden: true
    },
    component: NotFound
  }
]


// module.exports = routes;

export default routes