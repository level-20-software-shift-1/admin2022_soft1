// import { Random } from 'mockjs'

// let getAll = () => {
//     let arr = [];
//     for (let i = 0; i < 10; i++) {
//         arr.push({
//             username: Random.cname(),
//             password: Random.boolean(),
//             avatar: Random.image(),
//             remarks: Random.cparagraph(),
//         })
//     }
//     return arr;

// }

// // 如果在import导入中需要使用类似解构赋值的语法，则在暴露的时候不使用default 那么在import中就可以使用使用类似{getAll}的语法，来获得对应的值
// // 如果有default 那么在important的时候，这个对象实际是在被挂载在export.default.XXX中的
// export {getAll}
let arr=[];

export function getAll() {
    return {
        url: '/user/avatar',
        type: 'get',
        template: function () {


            return arr;
        }
    }
}