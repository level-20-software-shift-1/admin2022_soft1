import { Random } from 'mockjs'

let arr = []


for (let i = 0; i < 10; i++) {
    arr.push({
        id: Random.uuid(),
        cateName: Random.cname(),
        isActived: Random.boolean(),
        updatedTime: Random.datetime(),
        remarks: Random.paragraph(),
    })
}


// 如果在import导入中需要使用类似解构赋值的语法，则在暴露的时候不使用default 那么在import中就可以使用使用类似{getAll}的语法，来获得对应的值
// 如果有default 那么在important的时候，这个对象实际是在被挂载在export.default.XXX中的
// export default {
//     url: '/article/category',
//     type: 'get',
//     template: getAll
// }

// 获取分类的列表数据
export function getAll() {
    return {
        url: '/article/category',
        type: 'get',
        template: function () {
            // console.log(req);
            return {
                code: 1000,
                data: arr,
                msg: '获取数据成功'
            };
        }
    }
}

//删除指定id的分类
export function delCate() {
    return {
        url: RegExp('/article/category' + '/*'),
        type: 'delete',
        template: function (req) {
            // console.log(req);
            let id = req.url.replace('/article/category/', '')
            // console.log(id);

            // 模拟删除数据库表的数据
            arr = arr.filter(item => {
                return item.id !== id;
            })
            return {
                code: 1000,
                data: { id },
                msg: '删除数据成功'
            };
        }
    }
}

//新增文章分类
export function addCate() {
    return {
        url: RegExp('/article/category'),
        type: 'post',
        template: function (req) {
            // console.log(req);
            let obj = JSON.parse(req.body);
            // console.log(obj);

            let newRow = {
                id: Random.uuid(),
                cateName: obj.cateName,
                isActived: true,
                updatedTime: new Date().format('yyyy-MM-dd hh:mm:ss'),
                remarks: Random.cparagraph(),
            }

            arr.push(newRow);

            // 模拟删除数据库表的数据
            return {
                code: 1000,
                data: newRow,
                msg: '新增数据成功'
            };
        }
    }
}


//修改文章分类
export function editCate() {
    return {
        url: RegExp('/article/category'),
        type: 'put',
        template: function (req) {
            // console.log(req);
            let obj = JSON.parse(req.body);
            console.log(obj);

            let idx=arr.findIndex(item=>{
                return item.id===obj.id;
            })

            console.log(idx);

            arr[idx].cateName=obj.cateName;



            // 模拟删除数据库表的数据
            return {
                code: 1000,
                data: arr[idx],
                msg: '更新数据成功'
            };
        }
    }
}