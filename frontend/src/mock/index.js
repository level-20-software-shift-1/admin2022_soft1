const { mock } = require("mockjs");

/*
    引入fs失败引发的思考：
    1、这个项目里面，如何引入fs，才是可行
    2、这个项目里面，有没有其它方式，让我们可以遍历文件夹

*/


// 找到mock下modules下的所有js文件
let files= require.context("@/mock/modules", true, /\.js$/).keys();

files.forEach(item=>{
    // tmpFile是引入的模块,是一个对象,遍历这个对象的属性，这些属性每一个都是一个方法
    let tmpFile=require('./modules/'+item.slice(2));
    for(let key in tmpFile){
        let obj=tmpFile[key]();
        // console.log(obj);
        // 使用mock来捕获（拦截）对应url、type的请求，返回template（template可以是个对象、字符串或者函数）
        mock(obj.url,obj.type,obj.template)
    }
    // console.log(tmpFile);
})


