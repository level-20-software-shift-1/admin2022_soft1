import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// 引入模拟数据，模拟数据将拦截对应的请求路径和请求方法，匹配到了之后，就返回相应的模拟数据
// import './mock'
// 引入日期时间工具包
import './utils/dateHelper'


Vue.config.productionTip = false
Vue.use(ElementUI);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

// app.use(router)