using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Application.Common.Interface;
using Admin2022Soft1.Infrastructure.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Admin2022Soft1.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class FilesController : Controller
    {
        private readonly IAppFileUploadService _appFileService;

        public FilesController(IAppFileUploadService appFileService)
        {
            _appFileService = appFileService;
        }


        [HttpPost]
        public async Task<string> UploadFile(IFormCollection uploadFiles)
        {
            var list = await _appFileService.UploadFilesAsync(uploadFiles);

            if (list.Count() > 0)
            {
                var res = new
                {
                    Code = 1000,
                    Msg = "上传成功",
                    Data = list
                };

                return res.SerializeObject();
            }
            else
            {
                var res = new
                {
                    Code = 4000,
                    Msg = "上传失败，有不允许上传的文件扩展名或超出允许的文件大小",
                    Data = list
                };

                return res.SerializeObject();
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<FileContentResult> GetFile(Guid id)
        {
            var f = await _appFileService.GetFileByIdAsync(id);
            return f;
        }
    }
}