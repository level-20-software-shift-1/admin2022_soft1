using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Admin2022Soft1.Application.Common.Interface;

namespace Admin2022Soft1.Api.Service
{
    public class SessionUserService : ISessionUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SessionUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public Guid? UserId
        {
            get
            {
                var userIdString = _httpContextAccessor.HttpContext?.User.FindFirstValue("UserId");
                if (userIdString != null)
                {
                    var userId = new Guid(userIdString);
                    return userId;
                }
                else
                {
                    return null;
                }

            }
        }

        public string? Username => _httpContextAccessor.HttpContext?.User.FindFirstValue("Username");
    }
}