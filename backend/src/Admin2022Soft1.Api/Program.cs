using Admin2022Soft1.Api.Service;
using Admin2022Soft1.Application.Common.Interface;
using Admin2022Soft1.Infrastructure;
using Admin2022Soft1.Infrastructure.Filters;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
        });
});

// Add services to the container.

// 注册请求响应的上下文
builder.Services.AddHttpContextAccessor();

// 注册当前传话用户服务
builder.Services.AddTransient<ISessionUserService, SessionUserService>();
// 注册基础设施层中的所有实例
builder.Services.AddInfrastructure(builder.Configuration);

builder.Services.AddControllers(opt =>
{
    opt.Filters.Add(typeof(HttpGlobalExceptionFilter));
    opt.Filters.Add(typeof(AuditLogActionFilter));
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

// 种子数据
app.MigrateDatabase();

app.Run();
