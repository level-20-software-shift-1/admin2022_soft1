using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Application.Common.Interface;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Admin2022Soft1.Infrastructure
{
    public static class ApplicationStartupExtensions
    {
        public static async void MigrateDatabase(this WebApplication app)
        {
            using var scope = app.Services.CreateScope();
            var services = scope.ServiceProvider;

            try
            {
                var context = services.GetRequiredService<Admin2022Soft1DbContext>();

                // 自动同步迁移文件到数据库服务器，前提是必须先（手动）生成迁移文件
                context.Database.Migrate();

                var appUserRes = services.GetRequiredService<IRepository<AppUser>>();
                var appRoleRes = services.GetRequiredService<IRepository<AppRole>>();
                var appUserRoleRes = services.GetRequiredService<IRepository<AppUserRole>>();

                // 如果没有任何角色，则生成种子数据
                if (!appRoleRes.Table.Any())
                {
                    var role = new AppRole
                    {
                        RoleName = "超级管理员"
                    };
                    var user = new AppUser
                    {
                        Username = "admin",
                        Password = "113"
                    };
                    await appRoleRes.AddAsync(role);
                    await appUserRes.AddAsync(user);
                    await appUserRoleRes.AddAsync(new AppUserRole
                    {
                        AppRoleId = role.Id,
                        AppUserId = user.Id
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"An error occurred migrating the DB: {ex.Message}");
            }
        }
    }
}