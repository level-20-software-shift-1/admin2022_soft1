using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence.Configuration.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration
{
    public class AppFileInformationConfiguration : BaseEntityConfiguration<AppFileInformation>
    {
        public override void Configure(EntityTypeBuilder<AppFileInformation> builder)
        {
            base.Configure(builder);

            //设置表名
            builder.ToTable("app_file_information");

            builder.Property(x => x.OriginFileName).HasColumnName("origin_file_name").HasColumnType("text").HasColumnOrder(1);
            builder.Property(x => x.CurrentFileName).HasColumnName("current_file_name").HasColumnType("text").HasColumnOrder(2);
            builder.Property(x => x.RelativePath).HasColumnName("relative_path").HasColumnType("text").HasColumnOrder(3);
            builder.Property(x => x.FileSize).HasColumnName("file_size").HasColumnOrder(4);
            builder.Property(x => x.FileType).HasColumnName("file_type").HasColumnType("text").HasColumnOrder(5);
        }
    }
}