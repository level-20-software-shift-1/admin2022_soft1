using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Base;
using Microsoft.EntityFrameworkCore;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration.Base
{
    public class BaseEntityConfiguration<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id);

            // 以下对字段的顺序指定了顺序，数据越大越靠后，每个实体的顺序都是从1开始的，因为主键id已经是从0开始了
            builder.Property(x => x.Id).HasColumnName("id").HasColumnOrder(0);
            builder.Property(x => x.CreatedAt).HasColumnName("created_at").HasColumnOrder(992);
            builder.Property(x => x.UpdatedAt).HasColumnName("updated_at").HasColumnOrder(993);
            builder.Property(x => x.CreatedBy).HasColumnName("created_by").HasColumnOrder(994);
            builder.Property(x => x.UpdatedBy).HasColumnName("updated_by").HasColumnOrder(995);
            builder.Property(x => x.IsActived).HasColumnName("is_actived").HasColumnOrder(996);
            builder.Property(x => x.IsDeleted).HasColumnName("is_deleted").HasColumnOrder(997);
            builder.Property(x => x.DisplayOrder).HasColumnName("display_order").HasColumnOrder(998);
            builder.Property(t => t.Remarks).HasColumnName("remarks").HasColumnType("text").HasColumnOrder(999);
        }
    }
}