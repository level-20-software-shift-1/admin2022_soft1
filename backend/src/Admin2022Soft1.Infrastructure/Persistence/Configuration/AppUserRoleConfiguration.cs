using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence.Configuration.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration
{
    public class AppUserRoleConfiguration : BaseEntityConfiguration<AppUserRole>
    {
        public override void Configure(EntityTypeBuilder<AppUserRole> builder)
        {
            base.Configure(builder);

            //设置表名，查了老半天才查到原来设置表名方法是这个，残念。。。熬了个大夜
            builder.ToTable("app_user_role");

            builder.Property(x => x.AppUserId).HasColumnName("app_user_id").HasColumnOrder(1);
            builder.Property(x => x.AppRoleId).HasColumnName("app_role_id").HasColumnOrder(2);
        }
    }
}