using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence.Configuration.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration
{

    public class AppAuditLogConfiguration : BaseEntityConfiguration<AppAuditLog>
    {
        public override void Configure(EntityTypeBuilder<AppAuditLog> builder)
        {
            base.Configure(builder);

            //设置表名
            builder.ToTable("app_audit_log");

            builder.Property(x => x.Parameters).HasColumnName("parameters").HasColumnType("text").HasColumnOrder(1);
            builder.Property(x => x.BrowserInfo).HasColumnName("browser_info").HasColumnType("text").HasColumnOrder(2);
            builder.Property(x => x.ClientName).HasColumnName("client_name").HasColumnType("text").HasColumnOrder(3);
            builder.Property(x => x.ClientIpAddress).HasColumnName("client_ip_address").HasColumnType("text").HasColumnOrder(4);
            builder.Property(x => x.ExecutionDuration).HasColumnName("execution_duration").HasColumnOrder(5);
            builder.Property(x => x.ExecutionTime).HasColumnName("execution_time").HasColumnOrder(6);
            builder.Property(x => x.ReturnValue).HasColumnName("return_value").HasColumnType("text").HasColumnOrder(7);
            builder.Property(x => x.Exception).HasColumnName("exception").HasColumnType("text").HasColumnOrder(8);
            builder.Property(x => x.ServiceName).HasColumnName("service_name").HasColumnType("text").HasColumnOrder(9);
            builder.Property(x => x.UserInfo).HasColumnName("user_info").HasColumnType("text").HasColumnOrder(10);
            builder.Property(x => x.CustomData).HasColumnName("custom_data").HasColumnType("text").HasColumnOrder(11);
        }
    }
}