using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence.Configuration.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration
{
    public class AppExceptionLogConfiguration : BaseEntityConfiguration<AppExceptionLog>
    {
        public override void Configure(EntityTypeBuilder<AppExceptionLog> builder)
        {
            base.Configure(builder);

            //设置表名
            builder.ToTable("app_exception_log");

            builder.Property(x => x.ShortMessage).HasColumnName("short_message").HasColumnType("text").HasColumnOrder(1);
            builder.Property(x => x.FullMessage).HasColumnName("full_message").HasColumnType("text").HasColumnOrder(2);
        }
    }
}