using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Admin2022Soft1.Infrastructure.Persistence.Configuration.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Admin2022Soft1.Infrastructure.Persistence.Configuration
{
    public class AppIdentityUserConfiguration : BaseEntityConfiguration<AppIdentityUser>
    {
        public override void Configure(EntityTypeBuilder<AppIdentityUser> builder)
        {
            base.Configure(builder);

            //设置表名，查了老半天才查到原来设置表名方法是这个，残念。。。熬了个大夜
            builder.ToTable("app_identity_user");

            builder.Property(x => x.UserId).HasColumnName("user_id").HasColumnType("text").HasColumnOrder(1);
            builder.Property(x => x.Username).HasColumnName("user_name").HasColumnType("text").HasColumnOrder(2);
            builder.Property(x => x.RefreshToken).HasColumnName("refresh_token").HasColumnType("text").HasColumnOrder(3);
            builder.Property(x => x.RefreshTokenExpiration).HasColumnName("refresh_token_expiration").HasColumnOrder(4);
        }
    }
}