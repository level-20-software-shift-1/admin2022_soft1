using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Admin2022Soft1.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Admin2022Soft1.Infrastructure.Persistence
{
        public class Admin2022Soft1DbContext : DbContext
    {
        public Admin2022Soft1DbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // 应用当前Assembly中定义的所有的Configurations，就不需要一个一个去写了。
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }

        // 以下属性设置，直接设置为Set构造函数，以避免属性可能为空的警告（DbSet<Blog> Blog {get;set;}=null!)的形式也可行
        public DbSet<AppUser> AppUser => Set<AppUser>();
        public DbSet<AppRole> AppRole => Set<AppRole>();
        public DbSet<AppUserRole> AppUserRole => Set<AppUserRole>();
        public DbSet<AppAuditLog> AppAuditLog => Set<AppAuditLog>();
        public DbSet<AppExceptionLog> AppExceptionLog => Set<AppExceptionLog>();
        public DbSet<AppIdentityUser> AppIdentityUser => Set<AppIdentityUser>();
        public DbSet<AppFileInformation> AppFileInformation => Set<AppFileInformation>();
    }
}