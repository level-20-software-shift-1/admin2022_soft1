using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Application.Common.Interface;
using Admin2022Soft1.Domain.Base;
using Microsoft.EntityFrameworkCore;

namespace Admin2022Soft1.Infrastructure.Persistence.Repository
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly Admin2022Soft1DbContext _db;
        private readonly DbSet<T> _table;
        private readonly ISessionUserService _sessionUserService;

        public EfRepository(Admin2022Soft1DbContext db, ISessionUserService sessionUserService)
        {
            _db = db;
            _table = _db.Set<T>();
            _sessionUserService = sessionUserService;
        }

        public IQueryable<T> Table => _table.AsNoTracking();

        public async Task<T> AddAsync(T entity)
        {
            entity.CreatedAt = DateTime.UtcNow;
            entity.UpdatedAt = DateTime.UtcNow;
            entity.CreatedBy = _sessionUserService.UserId;
            entity.UpdatedBy = _sessionUserService.UserId;
            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.DisplayOrder = 0;
            await _table.AddAsync(entity);
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<T>> AddBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedAt = DateTime.UtcNow;
                entity.UpdatedAt = DateTime.UtcNow;
                entity.CreatedBy = _sessionUserService.UserId;
                entity.UpdatedBy = _sessionUserService.UserId;
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.DisplayOrder = 0;
            }
            await _table.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
            return entities;
        }

        public async Task DeleteAsync(Guid id, bool shouldHardDel = false)
        {
            var entity = await this.GetByIdAsync(id);
            if (entity != null)
            {
                await this.DeleteAsync(entity, shouldHardDel);
            }
        }

        public async Task DeleteAsync(T entity, bool shouldHardDel = false)
        {
            if (shouldHardDel)
            {
                _table.Remove(entity);
                await _db.SaveChangesAsync();
            }
            else
            {
                entity.IsDeleted = true;
                entity.UpdatedAt = DateTime.UtcNow;
                entity.UpdatedBy=_sessionUserService.UserId;
                _table.Update(entity);
                await _db.SaveChangesAsync();
            }
        }

        public async Task DeleteBulkAsync(IEnumerable<T> entities, bool shouldHardDel = false)
        {
            if (shouldHardDel)
            {
                _table.RemoveRange(entities);
                await _db.SaveChangesAsync();
            }
            else
            {
                foreach (var entity in entities)
                {
                    entity.IsDeleted = true;
                    // entity.UpdatedAt=DateTime.UtcNow;
                }
                await this.UpdateBulkAsync(entities);
            }
        }

        public async Task<T?> GetByIdAsync(Guid? id)
        {
            var entity = await _table.FindAsync(id);
            return entity;
        }

        public async Task UpdateAsync(T entity)
        {
            entity.UpdatedAt = DateTime.UtcNow;
            entity.UpdatedBy=_sessionUserService.UserId;

            _table.Update(entity);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.UpdatedAt = DateTime.UtcNow;
                entity.UpdatedBy=_sessionUserService.UserId;
            }
            _table.UpdateRange(entities);
            await _db.SaveChangesAsync();
        }
    }
}