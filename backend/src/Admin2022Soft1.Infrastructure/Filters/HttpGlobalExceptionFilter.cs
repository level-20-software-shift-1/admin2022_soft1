using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Application.Common.Interface;
using Admin2022Soft1.Domain.Entity;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Admin2022Soft1.Infrastructure.Filters
{
    // 全局捕获异常，保存到数据库
    public class HttpGlobalExceptionFilter : IAsyncExceptionFilter
    {
        private readonly IRepository<AppExceptionLog> _appExceptionLogRes;

        public HttpGlobalExceptionFilter(IRepository<AppExceptionLog> appExceptionLogRes)
        {
            _appExceptionLogRes = appExceptionLogRes;
        }


        public async Task OnExceptionAsync(ExceptionContext context)
        {
            //捕获全局异常，记入数据库日志
            await _appExceptionLogRes.AddAsync(new AppExceptionLog
            {
                ShortMessage = context.Exception.Message,
                FullMessage = context.Exception.ToString()
            });
            context.ExceptionHandled = true;
        }
    }
}