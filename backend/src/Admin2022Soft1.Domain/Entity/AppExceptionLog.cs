using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Domain.Entity
{
    public class AppExceptionLog : Base.BaseEntity
    {
        public string ShortMessage { get; set; }=null!;
        public string FullMessage { get; set; }=null!;
    }
}