using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Domain.Entity
{
 public class AppUserRole : Base.BaseEntity
    {
        public Guid AppUserId { get; set; }
        public Guid AppRoleId { get; set; }

        public virtual AppUser? AppUser { get; set; }
        public virtual AppRole? AppRole { get; set; }
    }
}