using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Domain.Entity
{
    public class AppFileInformation : Base.BaseEntity
    {
        public string OriginFileName { get; set; } = null!;
        public string CurrentFileName { get; set; } = null!;
        public string RelativePath { get; set; } = null!;
        public long FileSize { get; set; }
        public string FileType { get; set; } = null!;
    }
}