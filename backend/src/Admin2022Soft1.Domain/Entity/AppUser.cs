using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Domain.Entity
{
    public class AppUser : Base.BaseEntity
    {
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string? Nickname { get; set; }
        public Guid? Avatar { get; set; }
    }
}