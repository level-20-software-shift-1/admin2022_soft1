using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Application.RequestDto
{
    public class UserForAuthDto
    {
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}