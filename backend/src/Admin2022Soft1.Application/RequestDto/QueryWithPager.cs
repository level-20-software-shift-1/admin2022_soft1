using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Application.RequestDto
{
    public class QueryWithPager
    {
        public QueryWithPager()
        {
            Pager = new Pager();
        }
        public string? Keyword { get; set; }

        public Pager Pager { get; set; }
    }
}