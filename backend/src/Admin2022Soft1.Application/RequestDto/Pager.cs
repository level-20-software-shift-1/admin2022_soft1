using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Application.RequestDto
{
    public class Pager
    {
        private int _pageIndex;
        private int _pageSize;

        public Pager()
        {
            _pageIndex = 1;
            _pageSize = 10;
        }

        // 当前页码（当前第几页）
        public int PageIndex
        {
            get
            {
                if (_pageIndex < 1)
                {
                    _pageIndex = 1;
                }
                return _pageIndex;
            }
            set
            {
                _pageIndex = value;
            }
        }

        // 当前页大小（每页条数）
        public int PageSize
        {
            get
            {
                if (_pageSize < 10)
                {
                    _pageSize = 10;
                }
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        // 总条数
        public int RowsTotal { get; set; }
    }
}