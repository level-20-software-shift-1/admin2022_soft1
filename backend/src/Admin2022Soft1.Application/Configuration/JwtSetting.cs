using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Application.Configuration
{
    /// <summary>
    /// 用于从配置文件中获取Jwt的配置，以方便使用
    /// </summary>
    public class JwtSetting
    {
        public string Secret { get; set; } = null!;
        public string Issuer { get; set; } = null!;
        public string Audience { get; set; } = null!;
        public long AccessExpiration { get; set; }
        public long RefreshExpiration { get; set; }
    }
}