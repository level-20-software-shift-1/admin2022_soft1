using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin2022Soft1.Application.Common.Interface
{
    public interface IAppFileUploadService
    {
        Task<IEnumerable<Guid>> UploadFilesAsync(IFormCollection files);
        Task<FileContentResult> GetFileByIdAsync(Guid id);
    }
}