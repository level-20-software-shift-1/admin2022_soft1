using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Admin2022Soft1.Application.RequestDto;
using Admin2022Soft1.Application.ResponseDto;

namespace Admin2022Soft1.Application.Common.Interface
{
    public interface IIdentityService
    {
        bool ValidateUserAsync(UserForAuthDto userForAuth);

        Task<AppTokenDto> GenerateTokenAsync();

        Task<AppTokenDto?> RefreshTokenAsync(AppTokenDto appToken);
    }
}