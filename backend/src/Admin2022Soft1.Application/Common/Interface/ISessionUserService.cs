using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin2022Soft1.Application.Common.Interface
{
    public interface ISessionUserService
    {
        Guid? UserId { get; }
        string? Username { get; }
    }
}